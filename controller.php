<?php
require __DIR__.'/vendor/autoload.php';

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

error_reporting(E_ALL);
if(isset($_POST['action'])){
    $action = $_POST['action'];
}else{
    $action ='';
}

switch ($action) {
    case 'add_apartment':
        add_apartment();
        break;
    
    case 'view_all_apartments':
        view_all_apartments();
        break;

    case 'edit_apartment':
        edit_apartment();
        break;

    case 'delete_apartment':
        delete_apartment();
        break;

    case 'view_all_apartments_front':
        view_all_apartments_front();
        break;

    case 'add_rating':
        add_rating();
        break;

    case 'view_all_ratings':
        view_all_ratings();
        break;

    case 'edit_rating':
        edit_rating();
        break;

    case 'delete_rating':
        delete_rating();
        break;
}


function add_apartment(){


    $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/google-service-account.json');

    $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        // The following line is optional if the project id in your credentials file
        // is identical to the subdomain of your Firebase project. If you need it,
        // make sure to replace the URL with the URL of your project.
        ->withDatabaseUri('https://mockingjay-on-fire.firebaseio.com')
        ->create();

    $database = $firebase->getDatabase();

    $newPost = $database
        ->getReference('Apartments')
        ->push([
            'apartment_name' => $_POST['apartment_name'],
            'apartment_prize' => $_POST['apartment_prize'],
            'apartment_latitude' => $_POST['apartment_latitude'],
            'apartment_longitude' => $_POST['apartment_longitude']
        ]);

    $key = $newPost->getKey(); //print_r($key);// => -KVr5eu8gcTv7_AHb-3-
    $uri = $newPost->getUri(); //print_r($uri);// => https://my-project.firebaseio.com/blog/posts/-KVr5eu8gcTv7_AHb-3-

    $value2 = $newPost->getValue(); //print_r($value2);
    header('Location: admin/apartments/');
    exit();

}

function view_all_apartments(){
    $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/google-service-account.json');

    $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        // The following line is optional if the project id in your credentials file
        // is identical to the subdomain of your Firebase project. If you need it,
        // make sure to replace the URL with the URL of your project.
        ->withDatabaseUri('https://mockingjay-on-fire.firebaseio.com')
        ->create();

    $database = $firebase->getDatabase();

    $reference = $database->getReference('Apartments'); 
    $snapshot = $reference->getSnapshot();

    $apartments = $snapshot->getValue(); print_r($apartments);

    $out = '';
    foreach ($apartments as $key => $apartment) {
       $out .= '<tr id='. $key.'>
                    <td>'.$apartment['apartment_name'].'</td>
                    <td>'.$apartment['apartment_prize'].'</td>
                    <td>'.$apartment['apartment_latitude'].'</td>
                    <td>'.$apartment['apartment_longitude'].'</td>
                    <td>'.get_rating_by_apartment_key($key).'</td>
                    <td><a class="btn btn-primary" href="edit.php?key='.$key.'" role="button">Edit</a> | <a class="btn btn-danger" href="javascript:;" onclick=deleteApartment("'.$key.'"); role="button">Delete</a></td>
                  </tr>';
    }
    echo $out;

   
}

function get_apartment_data($key){

    $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/google-service-account.json');

    $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        // The following line is optional if the project id in your credentials file
        // is identical to the subdomain of your Firebase project. If you need it,
        // make sure to replace the URL with the URL of your project.
        ->withDatabaseUri('https://mockingjay-on-fire.firebaseio.com')
        ->create();

    $database = $firebase->getDatabase();

    $reference = $database->getReference('Apartments/'.$key); 
    $snapshot = $reference->getSnapshot();

    return $apartment = $snapshot->getValue();
}

function get_all_apartments(){

    $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/google-service-account.json');

    $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        // The following line is optional if the project id in your credentials file
        // is identical to the subdomain of your Firebase project. If you need it,
        // make sure to replace the URL with the URL of your project.
        ->withDatabaseUri('https://mockingjay-on-fire.firebaseio.com')
        ->create();

    $database = $firebase->getDatabase();

    $reference = $database->getReference('Apartments'); 
    $snapshot = $reference->getSnapshot();

    return $apartment = $snapshot->getValue();
}

function edit_apartment(){

    $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/google-service-account.json');

    $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        // The following line is optional if the project id in your credentials file
        // is identical to the subdomain of your Firebase project. If you need it,
        // make sure to replace the URL with the URL of your project.
        ->withDatabaseUri('https://mockingjay-on-fire.firebaseio.com')
        ->create();

    $database = $firebase->getDatabase();

    $key = $_POST['key'];

    $reference = $database->getReference('Apartments/'.$key); //print_r($reference);
    $reference->getChild('apartment_name')->set($_POST['apartment_name']);
    $reference->getChild('apartment_prize')->set($_POST['apartment_prize']);
    $reference->getChild('apartment_latitude')->set($_POST['apartment_latitude']);
    $reference->getChild('apartment_longitude')->set($_POST['apartment_longitude']);
    $value2 = $reference->getValue(); //print_r($value2);
    header('Location: admin/apartments/');
    exit();
}

function delete_apartment(){

    $key = $_POST['key'];

    $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/google-service-account.json');

    $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        // The following line is optional if the project id in your credentials file
        // is identical to the subdomain of your Firebase project. If you need it,
        // make sure to replace the URL with the URL of your project.
        ->withDatabaseUri('https://mockingjay-on-fire.firebaseio.com')
        ->create();

    $database = $firebase->getDatabase();
    $database->getReference('Apartments/'.$key)->remove();
    echo 200;
    
}

function add_rating(){


    $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/google-service-account.json');

    $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        // The following line is optional if the project id in your credentials file
        // is identical to the subdomain of your Firebase project. If you need it,
        // make sure to replace the URL with the URL of your project.
        ->withDatabaseUri('https://mockingjay-on-fire.firebaseio.com')
        ->create();

    $database = $firebase->getDatabase();

    $newPost = $database
        ->getReference('Ratings')
        ->push([
            'key' => $_POST['apartment_name'],
            'rating' => $_POST['rating']
            
        ]);

    $key = $newPost->getKey(); //print_r($key);// => -KVr5eu8gcTv7_AHb-3-
    $uri = $newPost->getUri(); //print_r($uri);// => https://my-project.firebaseio.com/blog/posts/-KVr5eu8gcTv7_AHb-3-

    $value2 = $newPost->getValue(); //print_r($value2);
    header('Location: admin/ratings/');
    exit();

}

function view_all_ratings(){
    $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/google-service-account.json');

    $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        // The following line is optional if the project id in your credentials file
        // is identical to the subdomain of your Firebase project. If you need it,
        // make sure to replace the URL with the URL of your project.
        ->withDatabaseUri('https://mockingjay-on-fire.firebaseio.com')
        ->create();

    $database = $firebase->getDatabase();

    $reference = $database->getReference('Ratings'); 
    $snapshot = $reference->getSnapshot();

    $ratings = $snapshot->getValue(); //print_r($ratings);



    $out = '';
    foreach ($ratings as $key => $rating) {
        $reference2 = $database->getReference('Apartments/'.$rating['key']); 
        $snapshot2 = $reference2->getSnapshot();
        $apartment = $snapshot2->getValue(); //print_r($apartment);

        $out .= '<tr id='. $key.'>
                    <td>'.$apartment['apartment_name'].'</td>
                    <td>'.$rating['rating'].'</td>
                    <td><a class="btn btn-primary" href="edit.php?key='.$key.'" role="button">Edit</a> | <a class="btn btn-danger" href="javascript:;" onclick=deleteRating("'.$key.'"); role="button">Delete</a></td>
                  </tr>';
    }
    echo $out;
   
}

function get_rating_data($key){

    $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/google-service-account.json');

    $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        // The following line is optional if the project id in your credentials file
        // is identical to the subdomain of your Firebase project. If you need it,
        // make sure to replace the URL with the URL of your project.
        ->withDatabaseUri('https://mockingjay-on-fire.firebaseio.com')
        ->create();

    $database = $firebase->getDatabase();

    $reference = $database->getReference('Ratings/'.$key); 
    $snapshot = $reference->getSnapshot();

    return $apartment = $snapshot->getValue();
}

function edit_rating(){  //print_r($_POST); die();

    $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/google-service-account.json');

    $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        // The following line is optional if the project id in your credentials file
        // is identical to the subdomain of your Firebase project. If you need it,
        // make sure to replace the URL with the URL of your project.
        ->withDatabaseUri('https://mockingjay-on-fire.firebaseio.com')
        ->create();

    $database = $firebase->getDatabase();

    $key = $_POST['key'];

    $reference = $database->getReference('Ratings/'.$key); //print_r($reference);
    $reference->getChild('key')->set($_POST['apartment_name']);
    $reference->getChild('rating')->set($_POST['rating']);
    $value2 = $reference->getValue(); //print_r($value2);
    header('Location: admin/ratings/');
    exit();
}

function delete_rating(){

    $key = $_POST['key'];

    $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/google-service-account.json');

    $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        // The following line is optional if the project id in your credentials file
        // is identical to the subdomain of your Firebase project. If you need it,
        // make sure to replace the URL with the URL of your project.
        ->withDatabaseUri('https://mockingjay-on-fire.firebaseio.com')
        ->create();

    $database = $firebase->getDatabase();
    $database->getReference('Ratings/'.$key)->remove();
    echo 200;
    
}

function get_rating_by_apartment_key($apartment_key){

    $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/google-service-account.json');

    $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        // The following line is optional if the project id in your credentials file
        // is identical to the subdomain of your Firebase project. If you need it,
        // make sure to replace the URL with the URL of your project.
        ->withDatabaseUri('https://mockingjay-on-fire.firebaseio.com')
        ->create();

    $database = $firebase->getDatabase();

    $reference = $database->getReference('Ratings'); 
    $snapshot = $reference->getSnapshot();

    $ratings = $snapshot->getValue(); //print_r($ratings);

    $total_rating = 0;
    $rating_count = 0;
    $average_rating = 0;
    foreach ($ratings as $key => $rate) {
        if($rate['key']==$apartment_key){
            $total_rating += (int)$rate['rating'];
            $rating_count++;
        }
    }
    if($rating_count>0){
        $average_rating = round(($total_rating/$rating_count),1);
    }
    return $average_rating;
}