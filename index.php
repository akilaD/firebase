<!DOCTYPE html>
<html>
<head>
	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Mockingjay Apartments</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<body>

	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
	  <a class="navbar-brand" href="../index.php">Mockingjay</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	  
	</nav>

	<div class="container">
		<h2>Apartments</h2>
	  <div class="row">

	   
	    <div class="col-sm-12">
	    	
	    	<table class="table table-striped" >
			    <thead>
			      <tr>
			        <th>Name</th>
			        <th>Prize</th>
			        <th>Latitude</th>
			        <th>Longitude</th>
			         <th>Rating</th>
			      </tr>
			    </thead>
			    <tbody id="data-table">
			      
			    </tbody>
			  </table>
	    </div>
	  </div>
		
	</div>
	
	<script src="https://www.gstatic.com/firebasejs/5.7.3/firebase.js"></script>
	<script>
		//(function() {
		  // Initialize Firebase
		  var config = {
		    apiKey: "AIzaSyDw7YkW7O4iMRitl8mx3OOT-PA4BgQcILA",
		    authDomain: "mockingjay-on-fire.firebaseapp.com",
		    databaseURL: "https://mockingjay-on-fire.firebaseio.com",
		    projectId: "mockingjay-on-fire",
		    storageBucket: "mockingjay-on-fire.appspot.com",
		    messagingSenderId: "1069379826461"
		  };
		  firebase.initializeApp(config);

	</script>
    <script>

    	function get_rating(apartment_key){
			var content = '';
		    var total_rating = 0;
		    var rating_count = 0;
		    var average_rating = 0;
			dbRef.on('value', function(snapshot) {
		        var ratings = snapshot.child("Ratings").val(); console.log(ratings);
		        //$('#data-table').empty();
		        
		        for(var key in ratings){
		        	console.log(apartment_key + "==" + ratings[key]['key']);
		        	if(apartment_key==ratings[key]['key']){
		        		total_rating += parseInt(ratings[key]['rating']);
		        		rating_count++;
		        	}
		            
		        }

		      });
			console.log(total_rating+'='+rating_count);
			if(rating_count>0){
				average_rating = total_rating/rating_count;
				average_rating = average_rating.toFixed(1);
			}
			return average_rating;
		}

        var dbRef = firebase.database().ref();
		var startListening = function() {
		     dbRef.on('value', function(snapshot) {
		        var apartments = snapshot.child("Apartments").val(); console.log(apartments);
		        $('#data-table').empty();
		        var content = '';

		        for(var key in apartments){
		        	//console.log(apartments[key]);
		        	content += '<tr id="'+key+'">';
		            content += '<td>' + apartments[key]['apartment_name'] + '</td>'; //column1
		            content += '<td>' + apartments[key]['apartment_prize'] + '</td>';//column2
		            content += '<td>' + apartments[key]['apartment_latitude'] + '</td>';//column3
		            content += '<td>' + apartments[key]['apartment_longitude'] + '</td>';//column4
		             content += '<td>' + get_rating(key) + '</td>';//column4
		            content += '</tr>';
		        }
		 
		        $('#data-table').append(content);
		      });
		     
		}
		startListening();

		
		
    </script>

</body>
</html>