<!DOCTYPE html>
<html>
<head>
	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Mockingjay Apartments</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<body>

	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
	  <a class="navbar-brand" href="../index.php">Mockingjay</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	  <div class="collapse navbar-collapse" id="collapsibleNavbar">
	    <ul class="navbar-nav">
	      <li class="nav-item">
	        <a class="nav-link" href="../apartments/">View Apartments</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="../ratings/">View Rating</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="#">Link</a>
	      </li>    
	    </ul>
	  </div>  
	</nav>

	<div class="container">
		<h2>Apartment Rating</h2>
	  	<h4>View Rating</h4>
	  <div class="row">

	    <div class="col-sm-3">
	    	<ul class="nav nav-pills flex-column">
		        <li class="nav-item">
		          <a class="nav-link" href="add.php">Add New Rating</a>
		        </li>
		        <li class="nav-item">
		          <a class="nav-link active" href="index.php">View All Rating</a>
		        </li>
		      </ul>
	    </div>
	    <div class="col-sm-9">
	    	
	    	<table class="table table-striped" >
			    <thead>
			      <tr>
			        <th>Apartment</th>
			        <th>Rating</th>
			        <th>Action</th>
			      </tr>
			    </thead>
			    <tbody id="data-table">
			      
			    </tbody>
			  </table>
	    </div>
	  </div>
		
	</div>
	<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$.ajax({
				type:'POST',
				data:'&action=view_all_ratings',
				url:'../../controller.php',
				success: function(res){
					$('#data-table').html(res);
				}
			})
		});

		function deleteRating(key){
			$(document).ready(function(){
				$.ajax({
					type:'POST',
					data:'&action=delete_rating&key='+key,
					url:'../../controller.php',
					success: function(res){
						location.reload();
						//$('#data-table').html(res);
					}
				})
			});
		}
	</script>

</body>
</html>