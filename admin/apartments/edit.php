<?php 
require_once '../../controller.php';

$key = $_GET['key'];
$apartment = get_apartment_data($key);

?>

<!DOCTYPE html>
<html>
<head>
	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Mockingjay Apartments</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<body>

	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
	  <a class="navbar-brand" href="../index.php">Mockingjay</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	  <div class="collapse navbar-collapse" id="collapsibleNavbar">
	    <ul class="navbar-nav">
	      <li class="nav-item">
	        <a class="nav-link" href="../apartments/">View Apartments</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="../ratings/">View Rating</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="#">Link</a>
	      </li>    
	    </ul>
	  </div>  
	</nav>

	<div class="container" style="margin-top:30px">
		<h2>Apartments</h2>
	  	<h4>Edit an Apartment</h4>
	  <div class="row">

	    <div class="col-sm-3">
	    	<ul class="nav nav-pills flex-column">
		        <li class="nav-item">
		          <a class="nav-link active" href="add.php">Add New</a>
		        </li>
		        <li class="nav-item">
		          <a class="nav-link" href="index.php">View All</a>
		        </li>
		      </ul>
	    </div>
	    <div class="col-sm-9">

	    	<form action="../../controller.php" method="post">
			  <div class="form-group">
			    <label for="apartment_name">Name:</label>
			    <input type="text" class="form-control" name="apartment_name" id="apartment_name" value="<?php echo $apartment['apartment_name'] ?>">
			    <input type="hidden" class="form-control" name="action" value="edit_apartment" id="edit_apartment">
			    <input type="hidden" class="form-control" name="key" id="key" value="<?php echo $key ?>">
			  </div>
			  <div class="form-group">
			    <label for="apartment_prize">Prize:</label>
			    <input type="text" class="form-control" name="apartment_prize" id="apartment_prize" value="<?php echo $apartment['apartment_prize'] ?>">
			  </div>
			  <div class="form-group">
			    <label for="apartment_latitude">Latitude:</label>
			    <input type="text" class="form-control" name="apartment_latitude" id="apartment_latitude" value="<?php echo $apartment['apartment_latitude'] ?>">
			  </div>
			  <div class="form-group">
			    <label for="apartment_longitude">Longitude:</label>
			    <input type="text" class="form-control" name="apartment_longitude" id="apartment_longitude" value="<?php echo $apartment['apartment_longitude'] ?>">
			  </div>
			  
			  <button type="submit" class="btn btn-primary">Submit</button>
			</form>
	    </div>
	  </div>
		
	</div>

</body>
</html>